package com.hhw.localsocketsample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.hhw.localsocketsample.client.LocalClient;
import com.hhw.localsocketsample.service.LocalService;

import java.nio.charset.StandardCharsets;

public class MainActivity extends Activity implements View.OnClickListener{

    static {
        System.loadLibrary("localsocketsample");
    }

    private LocalService mLocalService = null;
    private LocalClient mLocalClient = null;

    private final String CONNECT_ADDRESS = "com.hhw.localsocket";
    //字符串内容并无限制，只要确保客户端和服务端一致，且和其他LocalSocket服务端不冲突即可。

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void showToast(final String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    public native boolean startService(String address);

    public native boolean startClient(String address);

    public native void ClientSend(byte[] data);

    public native void ServiceSend(byte[] data);
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_start_cpp_client:

                if (mLocalClient != null){
                    mLocalClient.close(null);
                    mLocalClient = null;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startClient(CONNECT_ADDRESS);
                    }
                }).start();

                break;
            case R.id.btn_start_cpp_service:
                if (mLocalService != null){
                    showToast("Java服务端已启动！");
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startService(CONNECT_ADDRESS);
                    }
                }).start();

                break;
            case R.id.btn_start_java_client:
                mLocalClient = new LocalClient(CONNECT_ADDRESS);
                mLocalClient.connect();
                break;
            case R.id.btn_start_java_service:
                if (mLocalService == null){
                    mLocalService = new LocalService(CONNECT_ADDRESS);
                }

                if (mLocalService.isClose()){
                    mLocalService.accept();
                }else{
                    showToast("服务已启动");
                }
                break;
            case R.id.btn_client_send:
                if (mLocalClient != null){
                    mLocalClient.send("Hello Service I am Java Client".getBytes(StandardCharsets.UTF_8));
                }else{
                    ClientSend("Hello Service I am JNI Client".getBytes(StandardCharsets.UTF_8));
                }
                break;
            case R.id.btn_service_send:
                if (mLocalService != null){
                    mLocalService.send("Hello Client I am Java Service".getBytes(StandardCharsets.UTF_8));
                } else{
                    ServiceSend("Hello Client I am JNI Service".getBytes(StandardCharsets.UTF_8));
                }
                break;

        }
    }
}