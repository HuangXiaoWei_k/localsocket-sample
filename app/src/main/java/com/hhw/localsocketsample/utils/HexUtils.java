package com.hhw.localsocketsample.utils;

public class HexUtils {
    public static String toHexStr(byte[] data,int offset,int len){
        StringBuilder builder = new StringBuilder();
        for (int i = offset; i < offset+len; i++) {
            byte b = data[i];
            String hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1) {
                builder.append("0");
            }

            builder.append(hex);
            builder.append(" ");
        }

        return builder.toString();
    }
}
