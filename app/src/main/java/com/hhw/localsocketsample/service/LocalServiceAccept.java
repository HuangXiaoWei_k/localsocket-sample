package com.hhw.localsocketsample.service;

import android.net.LocalSocket;
import com.hhw.localsocketsample.utils.HexUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LocalServiceAccept {
    private LocalSocket mSocket;
    private InputStream mInputStream;
    private OutputStream mOutputStream;

    private boolean isFLAG = false;
    private final ExecutorService mThreadPool = Executors.newFixedThreadPool(2);

    public LocalServiceAccept(LocalSocket mLocalSocket){
        isFLAG = true;
        this.mSocket = mLocalSocket;

        try {
            mInputStream = mSocket.getInputStream();
            mOutputStream = mSocket.getOutputStream();
            startRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startRead() {
        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                byte[] data = new byte[1024];

                while (isFLAG){
                    try {
                        if (mInputStream.available() <= 0){
                            try {
                                Thread.sleep(1000);
                            }catch (Exception ignored){}
                            continue;
                        }

                        int len = mInputStream.read(data);
                        if (len < 0){
                            break;
                        }
                        System.out.println("服务端收到数据："+ new String(data,0,len));
//                        System.out.println("十六进制数据："+ HexUtils.toHexStr(data,0,
//                                len));

                    }catch (Exception e){
                        close(e);
                    }
                }
            }
        });
    }

    /**
     * 发送数据
     * @param data
     */
    public void send(byte[] data){
        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                if (isConnect()){
                    try {
                        mOutputStream.write(data);
                    }catch (Exception e){
                        close(e);
                    }
                }
            }
        });
    }



    /**
     * 关闭连接
     * @param e
     */
    public void close(Exception e){
        isFLAG = false;

        if (e != null){
            e.printStackTrace();
        }

        close(mInputStream);
        mInputStream = null;
        close(mOutputStream);
        mOutputStream = null;
        close(mSocket);
        mSocket = null;

        //如果需要重连，可以在这里处理重连
    }

    private void close(Closeable cls){
        if (cls !=null){
            try {
                cls.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 当前是否为连接状态
     * @return
     */
    public boolean isConnect(){
        return mSocket!=null&&mSocket.isConnected()&&isFLAG;
    }
}
